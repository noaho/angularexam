import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {environment} from './../environments/environment';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from "./users.service";
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UsersfComponent } from './usersf/usersf.component';



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersfComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
     RouterModule.forRoot([
    {path: '', component: ProductsComponent},
    {path: 'users', component: UsersComponent},
    {path: 'products', component: ProductsComponent},
    {path:"usersf",component:UsersfComponent},
    {path: '**', component: NotFoundComponent}])
  ],
  providers: [UsersService,HttpModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
